# Code Challenge Description

The application will allow the user to search for restaurants nearby, save them to a favorites list and view their previously saved restaurants. The data is obtained by querying the Google Places API, and the saved restaurant data is stored in Firebase. 

![UI mock-up](https://i.ibb.co/F02xwj4/google-places-ui.png)

### Requirements 
- The application should request the user for their location. 
- The search bar should trigger querying the Google Places API on pressing the Enter key, using the keyword and location provided by the user. 
- The data should be displayed neatly in cards within a grid on the screen.
- Each card should have a save button that will store the data in Firebase.
- The Favorites button should trigger getting data from Firebase and replace the current places list.
- All API interactions (Google Places & Firebase) should be handled by a REST Node backend.
- Use Material UI for front end, React and Typescript.

